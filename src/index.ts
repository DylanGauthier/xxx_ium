import Vue from 'vue';

import store, { Actions } from './store';
import GameView from './views/game.vue';

new Vue({
    el: "#app",
    template: `
        <game-view />
    `,
    data: {
        name: "World"
    },
    store,
    components: {
        GameView,
    }
});

Vue.config.devtools = true;

setInterval(() => {
    store.dispatch(Actions.TICK);
}, 1000);