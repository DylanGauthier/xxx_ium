import Vue from 'vue';
import Vuex from 'vuex';

import { Extractor } from './entities/items/extractor';
import { Item } from './entities/items/item';
import { ItemType } from './entities/items/item.types';
import { Planet } from './entities/planets/planet';
import { StartingPlanet } from './entities/planets/starting-planet';
import { ResourceManager } from './entities/resources/resource.manager';
import {
    CollectedResource,
    ResourceType,
} from './entities/resources/resource.types';

export enum Mutations {
    SET_STARTING_PLANET = 'setStartingPlanet',
    SET_CURRENT_PLANET = 'setCurrentPlanet',
    CREATE_PLANET = 'createPlanet',
    CREATE_ITEM = 'createItem',
    TICK = 'tick',
}

export enum Actions {
    START_GAME = 'startGame',
    TICK = 'tick',
}

interface State {
    tickCount: number;
    planets: Planet[];
    items: Item[];
    collectedResources: CollectedResource;
    startingPlanet: StartingPlanet;
    currentPlanet: Planet;
}

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        tickCount: 0,
        planets: [],
        items: [],
        collectedResources: ResourceManager.getCollected(),
        startingPlanet: null,
        currentPlanet: null,
    },
    mutations: {
        [Mutations.SET_STARTING_PLANET](state: State, startingPlanet: StartingPlanet): void {
            console.log('setting starting planet!', startingPlanet);
            state.startingPlanet = startingPlanet;
        },
        [Mutations.SET_CURRENT_PLANET](state: State, planet: Planet): void {
            console.log('changing planet!', planet);
            state.currentPlanet = planet;
        },
        [Mutations.CREATE_PLANET](state: State, planet: Planet): void {
            console.log('Creating a new planet!', planet);
            state.planets.push(planet);
        },
        [Mutations.CREATE_ITEM](state: State, item: Item): void {
            console.log('Creating a new item!', item);
            state.items.push(item)
        },
        [Mutations.TICK](state: State): void {
            console.log('tick!');
            state.tickCount++;

            const extractors: Extractor[] = state
                .items
                .filter(item => item.getType() === ItemType.Extractor) as Extractor[];

            for (const extractor of extractors) {
                extractor.collect();
            }

            state.collectedResources = ResourceManager.getCollected();
        },
    },
    actions: {
        [Actions.START_GAME]({ commit }): void {
            const startingPlanet: StartingPlanet = new StartingPlanet();

            const copperExtractor: Extractor = new Extractor(startingPlanet);
            copperExtractor.setExtractingResource(ResourceType.Copper);
            commit(Mutations.CREATE_ITEM, copperExtractor);

            commit(Mutations.SET_STARTING_PLANET, startingPlanet);
            commit(Mutations.CREATE_PLANET, startingPlanet);
            commit(Mutations.SET_CURRENT_PLANET, startingPlanet);
        },
        [Actions.TICK]({ commit }): void {
            commit(Mutations.TICK);
        }
    },
    getters: {
        collected: (state) => state.collectedResources,
    }
});