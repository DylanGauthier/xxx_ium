export class Entity {
    private identifier: string;

    constructor(private readonly type: string) {
        this.identifier = Math.random().toString().replace('.', '');
    }

    public getIdentifier(): string {
        return this.identifier;
    }

    public getType(): string {
        return this.type;
    }
}
