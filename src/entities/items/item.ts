import { Entity } from '../entity';
import { Planet } from '../planets/planet';
import { ItemType } from './item.types';

export class Item extends Entity {
    protected level: number = 1;

    constructor(type: ItemType, private location: Planet) {
        super(type);
    }
    
    public getLocation(): Planet {
        return this.location;
    }

    public getLevel(): number {
        return this.level;
    }
}
