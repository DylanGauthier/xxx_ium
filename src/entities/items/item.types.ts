export enum ItemType {
    Extractor = 'EXTRACTOR',
    SpaceScanner = 'SPACE_SCANNER',
}
