import { Planet } from '../planets/planet';
import { Item } from './item';
import { ItemType } from './item.types';

export class SpaceScanner extends Item {
    constructor(location: Planet) {
        super(ItemType.SpaceScanner, location);
    }
}