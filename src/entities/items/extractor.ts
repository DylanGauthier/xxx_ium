import { Planet } from '../planets/planet';
import { Resource } from '../resources/resource';
import { ResourceManager } from '../resources/resource.manager';
import { ResourceType } from '../resources/resource.types';
import { Item } from './item';
import { ItemType } from './item.types';

export class Extractor extends Item {
    private outputPerTick: number = 1;
    private outputModifierPerLevel: number = 1.10;
    private extractingResource: ResourceType = null;

    constructor(location: Planet) {
        super(ItemType.Extractor, location);
    }

    public setExtractingResource(resourceType: ResourceType): void {
        this.extractingResource = resourceType;
    }

    public getExtractingResource(): ResourceType {
        return this.extractingResource;
    }

    public getDescription(): string {
        return `Currently extracting ${this.extractingResource} at a speed of ${this.getOutputPerTick()} per tick`;
    }

    public getOutputPerTick(): number {
        return this.outputPerTick;
    }

    public levelUp(): void {
        this.outputPerTick = Math.round((this.outputPerTick * this.outputModifierPerLevel) * 100) / 100;
        this.level++;
    }
    
    public collect(): void {
        const resource: Resource = this.getLocation().getResources().find(resource => resource.getType() === this.getExtractingResource());

        resource.setQuantity(resource.getQuantity() - this.getOutputPerTick());
        ResourceManager.increment(resource.getType() as ResourceType, this.getOutputPerTick());
    }
}
