import { Entity } from '../entity';
import { ResourceType } from './resource.types';

export class Resource extends Entity {
    constructor(type: ResourceType, private quantity: number) {
        super(type);
    }

    public getQuantity(): number {
        return this.quantity;
    }

    public setQuantity(qty: number): void {
        this.quantity = qty;
    }
}