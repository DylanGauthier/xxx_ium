import {
    ResourceType,
    UnitOfMeasure,
} from './resource.types';

export const RESOURCE_CONFIG = {
    [ResourceType.AluminiumAlloy]: {
        unitOfMeasure: UnitOfMeasure.Ton,
    },
    [ResourceType.Magnesium]: {
        unitOfMeasure: UnitOfMeasure.Ton,
    },
    [ResourceType.Uranium]: {
        unitOfMeasure: UnitOfMeasure.Ton,
    },
    [ResourceType.Copper]: {
        unitOfMeasure: UnitOfMeasure.Ton,
    },
    [ResourceType.Titanium]: {
        unitOfMeasure: UnitOfMeasure.Ton,
    },
    [ResourceType.Water]: {
        unitOfMeasure: UnitOfMeasure.CubicMeter,
    },
}
