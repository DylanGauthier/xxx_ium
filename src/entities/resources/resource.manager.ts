import {
    CollectedResource,
    ResourceType,
} from './resource.types';

export class ResourceManager {
    private static collected: CollectedResource = {
        [ResourceType.AluminiumAlloy]: 0,
        [ResourceType.Copper]: 0,
        [ResourceType.Magnesium]: 0,
        [ResourceType.Titanium]: 0,
        [ResourceType.Uranium]: 0,
        [ResourceType.Water]: 0,
    };

    public static increment(resourceType: ResourceType, qty: number): void {
        this.collected[resourceType] = this.collected[resourceType] + qty;
    }

    public static decrement(resourceType: ResourceType, qty: number): void {
        this.collected[resourceType] = this.collected[resourceType] - qty;
    }

    public static getCollected(): CollectedResource {
        return this.collected;
    }
}
