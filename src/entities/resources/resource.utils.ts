import { RESOURCE_CONFIG } from './resource.config';
import {
    ResourceType,
    UnitOfMeasure,
} from './resource.types';

export class ResourceUtils {
    public static getHumanReadableQuantity(type: ResourceType, qty: number): string {
        const s: number = this.roundQuantity((qty / 1000) / 1000);
        return `${s} k/${this.getUnitOfMeasure(type)} (${this.roundQuantity(qty)})`;
    }

    public static roundQuantity(qty: number): number {
        return Math.round(qty * 100) / 100;
    }

    public static getUnitOfMeasure(type: ResourceType): UnitOfMeasure {
        return RESOURCE_CONFIG[type].unitOfMeasure;
    }
}