import { Resource } from './resource';
import { ResourceType } from './resource.types';

export class Copper extends Resource {
    constructor(qty: number) {
        super(ResourceType.Copper, qty);
    }
}