import { Resource } from './resource';
import { ResourceType } from './resource.types';

export class Water extends Resource {
    constructor(qty: number) {
        super(ResourceType.Water, qty);
    }
}