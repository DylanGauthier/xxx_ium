import { Resource } from './resource';
import { ResourceType } from './resource.types';

export class Magnesium extends Resource {
    constructor(qty: number) {
        super(ResourceType.Magnesium, qty);
    }
}