export enum ResourceType {
    AluminiumAlloy = 'ALUMINIUM_ALLOY',
    Magnesium = 'MAGNESIUM',
    Uranium = 'URANIUM',
    Copper = 'COPPER',
    Titanium = 'TITANIUM',
    Water = 'WATER',
}

export enum UnitOfMeasure {
    CubicMeter = 'm3',
    Ton = 't'
}

export type CollectedResource = { [k in ResourceType]: number };