import { Resource } from './resource';
import { ResourceType } from './resource.types';

export class Titanium extends Resource {
    constructor(qty: number) {
        super(ResourceType.Titanium, qty);
    }
}