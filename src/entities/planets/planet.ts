import { getRandomBetweenRange } from '../../utils';
import { Entity } from '../entity';
import { Resource } from '../resources/resource';

export class Planet extends Entity {
    private name: string;

    constructor(
        private scanPercent: number = 0,
        private resources: Resource[] = [],
    ) {
        super('PLANET');

        this.name = this.generateName();
    }

    public getName(): string {
        return this.name;
    }

    public getScanPercent(): number {
        return this.scanPercent;
    }

    public getResources(): Resource[] {
        return this.resources;
    }

    private generateName(): string {
        const alphabet: string = 'ABCDEFGHIJKLMNOPQSTUVWXYZ';
        const numbers: string = '0123456789';

        let n: string = '';

        for (let i: number = 0; i < getRandomBetweenRange(4, 7); i++) {
            n += alphabet[getRandomBetweenRange(0, alphabet.length - 1)];
        }

        n += '-';

        for (let i: number = 0; i < getRandomBetweenRange(4, 7); i++) {
            n += numbers[getRandomBetweenRange(0, numbers.length - 1)]
        }

        return n;
    }
}
