import { Copper } from '../resources/copper.resource';
import { Magnesium } from '../resources/magnesium.resource';
import { Titanium } from '../resources/titanium.resource';
import { Water } from '../resources/water.resource';
import { Planet } from './planet';

export class StartingPlanet extends Planet {
    constructor() {
        super(
            100,
            [
                new Copper(15000000),
                new Magnesium(15000000),
                new Titanium(15000000),
                new Water(15000000),
            ]
        );
    }
}